# README #

### How do I get set up? ###

To configure the project for OpenCL development:

* Get babl
* Create a config.site file with babl cpath and ldpath (using pkgconfig)
* Store the config.site in your prefix
* Run the following command (from: [gnome developer's build](https://mail.gnome.org/archives/gegl-developer-list/2012-December/msg00001.html))


```
#!sh

$ ./autogen.sh --enable-debug --prefix=$my_prefix
$ make
```


###How do I test if my OpenCL implementation is correct?###

```
#!sh

$ cd tests/compositions
$ GEGL_DEBUG=opencl GEGL_USE_OPENCL=yes python run-compositions.py alien-map.xml
```

###How do I test if my OpenCL implementation is fast?###

```
#!bash

$ cd tests/compositions/
$ perl get_process_time.pl alien-map.xml <cpu|gpu|no>
```

### Extra info ###

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
* Writing tests
* Other guidelines