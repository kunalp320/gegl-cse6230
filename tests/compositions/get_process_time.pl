#!/usr/bin/perl
use strict;
use warnings;

# Usage
# perl get_process_time.pl [operation] <cpu|gpu|no> [#iterations] [make?]
# DEPRECATED: perl get_process_time.pl [executable] [operation] <cpu|gpu|no>

# Run gegl given inputs
#my $executable = $ARGV[0]; #'./bin/gegl ';
my $executable = '../../bin/gegl ';

my $operation = $ARGV[0]; #'tests/compositions/stretch-contrast.xml ';

# If no cl_device is specified, assume OpenCL is disabled
my $cl_device = $ARGV[1]; # gpu, cpu, no;
my $iterations = $ARGV[2]; # gpu, cpu, no;
# If this argument isn't specified, we will make by default
if (!$ARGV[3]) {
    system("make", "-C", "../../");
}

# Write to a null image so as to not bring up the gtk viewer
my $output = '-o /dev/null/a.png';

$ENV{"GEGL_DEBUG_TIME"} = 1;
$ENV{"GEGL_USE_OPENCL"} = $cl_device;

my $avg_tot_time = 0;
my $avg_process_time = 0;
my $avg_op_time = 0;
for(my $i = 0; $i < $iterations; ++$i) {
    my @timing = `$executable $operation $output`;
    print @timing; # uncomment to double check output
    #print ($executable, $operation, $output); # uncomment to double check output


    # Process the GEGL_DEBUG_TIME output for the times of interest
    # gegl outputs the total time and then routines that take the
    # the most time to the least.
    my $tot_time = 0;
    my $process_time = 0;
    my $op_time = 0;
    while (my $line = shift @timing) {

        # Regex that captures float variables
        my $float_var = qr/[0-9]+\.[0-9]+/;

        # Capture the total time in seconds
        if ($line =~ /(${float_var})s/) {
            $tot_time = $1;
        }
        # Capture the processing time percentage
        elsif ($line =~ /process\s+(${float_var})%/) 
        {
            my $process_percent = $1;
            $process_time = $tot_time*$process_percent/100;

            # Capture the operation time percentage
            # This will be correct most of the time since the op should 
            # take longer than the aux processing tasks
            my $next_line = shift @timing;
            if ($next_line =~ /(${float_var})%/) {
                my $op_percent = $1;
                $op_time = $process_time*$op_percent/100;
                last;
            }
        }
    }
    print "Intermediate Total = ", $tot_time, "s\n";
    print "Intermediate Process = ", $process_time, "s\n";
    print "Intermediate Operation = ", $op_time, "s\n";
    $avg_tot_time += $tot_time;
    $avg_process_time += $process_time;
    $avg_op_time += $op_time;
}

# Print times of interest
print "Average Time Breakdown:\n";
print "Total = ", $avg_tot_time/$iterations, "s\n";
print "Process = ", $avg_process_time/$iterations, "s\n";
print "Operation = ", $avg_op_time/$iterations, "s\n";

